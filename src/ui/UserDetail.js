import { Dimensions, FlatList, Image, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { primaryColor } from '../helper/ColorHelper';
import Spacer from '../helper/Spacer';
import { useEffect, useState } from 'react';
import DatePicker from 'react-native-date-picker';
import moment from "moment";


export default function UserDetail({ route, navigation }) {
    const { firstNameParam, lastNameParam, emailParam, dobParam } = route.params;
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const [email, setEmail] = useState('');
    const [dob, setDob] = useState(new Date());

    const [open, setOpen] = useState(open);



    useEffect(() => {
        console.log(`dobParam: ${dobParam}`);
        setFirstName(firstNameParam);
        setLastName(lastNameParam);
        setEmail(emailParam);
        setDob(moment(Date(dobParam)).format('DD MMM YYYY'));
    }, [])
    return (
        <SafeAreaView>
            <SaveAndCancelBuilder navigation={navigation} />
            <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height, backgroundColor: 'white' }}>
                <MainInformationBuilder firstName={firstName} setFirstName={setFirstName} lastName={lastName} setLastName={setLastName} />
                <Spacer height={10} />
                <SubInformationBuilder email={email} setEmail={setEmail} dob={dob} setDob={setDob} open={open} setOpen={setOpen} />
            </View>
        </SafeAreaView>
    );
}

const SaveAndCancelBuilder = (props) => {
    return (
        <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height * 0.10, backgroundColor: 'white', flexDirection: 'row', paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => props.navigation.pop()} style={{ flex: 1, alignItems: 'flex-start' }}>
                <Text style={{ color: primaryColor }}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end' }}>
                <Text style={{ color: primaryColor }}>Save</Text>
            </TouchableOpacity>
        </View>
    );
}


const MainInformationBuilder = (props) => {

    return (
        <View style={{ width: Dimensions.get('window').width, backgroundColor: 'white', flexDirection: 'column' }}>
            <View style={{ height: 25, backgroundColor: 'grey', flexDirection: 'row', alignItems: 'center', paddingStart: 10, paddingEnd: 10 }}>
                <Text style={{ fontWeight: 'bold', justifyContent: 'center' }}>Main Information</Text>
            </View>
            <Spacer height={10} />
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingStart: 10, paddingEnd: 10 }}>
                <View style={{ width: 80 }}>
                    <Text>First Name</Text>
                </View>
                <TextInput onChangeText={(e) => props.setFirstName(e)} value={props.firstName} style={{
                    flex: 1,
                    height: 40,
                    borderWidth: 1,
                    padding: 10,
                }} />
            </View>
            <Spacer height={10} />
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingStart: 10, paddingEnd: 10 }}>
                <View style={{ width: 80 }}>
                    <Text>Last Name</Text>
                </View>
                <TextInput onChangeText={(e) => props.setLastName(e)} value={props.lastName} style={{
                    flex: 1,
                    height: 40,
                    borderWidth: 1,
                    padding: 10,
                }} />
            </View>
        </View>
    );
}

const SubInformationBuilder = (props) => {
    
    return (
        <View style={{ width: Dimensions.get('window').width, backgroundColor: 'white', flexDirection: 'column' }}>
            <View style={{ height: 25, backgroundColor: 'grey', flexDirection: 'row', alignItems: 'center', paddingStart: 10, paddingEnd: 10 }}>
                <Text style={{ fontWeight: 'bold' }}>Sub Information</Text>
            </View>
            <Spacer height={10} />
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingStart: 10, paddingEnd: 10 }}>
                <View style={{ width: 80 }}>
                    <Text>Email</Text>
                </View>
                <View style={{ width: 5 }} />
                <TextInput onChangeText={(e) => props.setEmail(e)} value={props.email} style={{
                    flex: 1,
                    height: 40,
                    borderWidth: 1,
                    padding: 10,
                }} />
            </View>
            <Spacer height={10} />
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingStart: 10, paddingEnd: 10 }}>
                <View style={{ width: 80 }}>
                    <Text>DOB</Text>
                </View>
                <View style={{ width: 5 }} />
                <TouchableOpacity onPress={() => props.setOpen(true)} style={{
                    flex: 1,
                    height: 40,
                    borderWidth: 1,
                    padding: 10,
                }}>
                    <Text>{props.dob.toString()}</Text>
                </TouchableOpacity>

            </View>

            <DatePicker
                modal
                open={props.open}
                date={new Date()}
                onConfirm={(date) => {
                    // props.setOpen(false);
                    // props.setDob(showDate)
                    console.log(`choose date: ${date}`);


                }}
                onCancel={() => {
                    props.setOpen(false);
                }}
            />

        </View>
    );
}