import React, { useContext } from "react";
import { Dimensions, FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import { Context } from "../controller/provider";

export default function UserListPage({ navigation }) {
    const [globalState, globalDispatch] = useContext(Context);
    console.log(`globalState.stateListUser: ${JSON.stringify(globalState.stateListUser)}`)
    return (
        <View style={{ backgroundColor: 'white', width: Dimensions.get('window').width, height: Dimensions.get('window').height }}>
            <FlatList
                data={globalState.stateListUser}
                renderItem={({ item }) => <UserItem item={item} navigation={()=>navigation.navigate('UserDetail',{
                    firstNameParam: item.firstName,
                    lastNameParam: item.lastName, 
                    emailParam: item.email,
                    dobParam: item.dob
                })} />}
                numColumns={2}
                keyExtractor={item => item.id}
            />
        </View>
    );
}

const UserItem = (props) => {
    console.log(`props: ${props.item}`)
    return (
        <TouchableOpacity style={{
            borderColor: 'grey',
            flex: 1,
            flexDirection: 'column',
            borderWidth: 1,
            marginStart: 5,
            marginEnd: 5,
            marginTop: 5

        }}
            onPress={props.navigation}
        >
            <View style={{ width: Dimensions.get('window').width / 2, height: 70, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                <View style={{
                    width: 70,
                    height: 70,
                    borderRadius: 70 / 2,
                    backgroundColor: '#FF9800',

                }} />

            </View>
            <View style={{ height: 10 }} />
            <View style={{ flex: 1, alignItems: 'center', marginBottom: 40 }}>
                <Text>
                    {props.item.firstName}  {props.item.lastName}
                </Text>
            </View>

        </TouchableOpacity>
    );
}