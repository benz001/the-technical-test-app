import { createContext, useMemo, useReducer } from "react";
import { combineReducers, listDataUser, listUserReducer} from "./reducer";

const Context = createContext();

const initialState = {
    stateListUser: listDataUser,
}

const rootReducer = combineReducers({
    stateOne: listUserReducer,
});

function Provider({ children }) {
    const [state, dispatch] = useReducer(rootReducer, initialState);
    const store = useMemo(() => [state, dispatch], [state]);
    return (
        <Context.Provider value={store}>
            {children}
        </Context.Provider>
    );

}

export {
    Context,
    Provider
}