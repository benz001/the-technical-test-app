import { GET_LIST,GET_DETAIL } from "./action";

export const listDataUser = require('../data/data.json');

export const listUserReducer = (state, action) => {
    switch (action.type) {
        case GET_LIST:
            console.log('get list');
            return state;
        case GET_DETAIL:
            console.log('get detail');
            state.findIndex((obj => obj.id == action.payload));
            return state;
        default:
            return state;
    }
}



export function combineReducers(slices) {
    return (state, action) =>
        Object.keys(slices).reduce(
            (acc, prop) => ({
                ...acc,
                [prop]: slices[prop](acc[prop], action)
            }),
            state
        )
}