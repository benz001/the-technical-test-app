export const GET_LIST = 'get_list';
export const GET_DETAIL = 'get_detail';

export const getList = (type) => {
    return {
        "type": type
    }
}

export const getDetail = (type, payload) => {
    return {
        "type": type,
        "id": payload
    }
}

