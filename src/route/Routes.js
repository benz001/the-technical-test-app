import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import UserListPage from '../ui/UserList';
import UserDetail from '../ui/UserDetail';

const Stack = createNativeStackNavigator();

export default function Routes() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{
                    headerShown: false
                }}>
                <Stack.Screen
                    name="UserList"
                    component={UserListPage}
                //   options={{title: ''}}
                />
                <Stack.Screen name="UserDetail" component={UserDetail}  />
            </Stack.Navigator>
        </NavigationContainer>
    );
};