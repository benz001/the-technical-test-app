import { View } from "react-native";

export default Spacer = (props) =>{
    return (
        <View style={{height: props.height}}/>
    );
}